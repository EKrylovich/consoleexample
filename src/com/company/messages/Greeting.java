package com.company.messages;


import java.util.function.Supplier;

public class Greeting implements Supplier<String> {
    @Override
    public String get() {
        return "What is your name?";
    }
}
