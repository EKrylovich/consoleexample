package com.company.messages;

import java.text.MessageFormat;
import java.util.function.Function;

public class TeaOrCoffee implements Function<String, String> {
    private final String TEA_OR_COFFEE = "Hi, {0}! welcome! you are in the shop, would you like tea or coffee?";

    @Override
    public String apply(final String name) {
        return MessageFormat.format(TEA_OR_COFFEE, name);
    }
}
