package com.company.messages;

import java.text.MessageFormat;
import java.util.function.Function;

public class DrinkOrLeave implements Function<String, String> {
    private final String DRINK_OR_LEAVE = "Here is your {0}, you want to drink it or leave it?";

    @Override
    public String apply(final String teaOrCoffee) {
        return MessageFormat.format(DRINK_OR_LEAVE, teaOrCoffee);
    }
}
