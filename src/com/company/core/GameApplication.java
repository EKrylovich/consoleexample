package com.company.core;

import com.company.messages.DrinkOrLeave;
import com.company.messages.Greeting;
import com.company.messages.TeaOrCoffee;

import java.util.Scanner;

public class GameApplication {
    private final Scanner in = new Scanner(System.in);

    public String greeting(){
        System.out.println(new Greeting().get());
        return in.nextLine();
    }

    public String teaOrCoffee(final String name) {
        System.out.println(new TeaOrCoffee().apply(name));
        return in.nextLine();
    }

    public String drinkOrLeave(final String teaOrCoffee) {
        System.out.println(new DrinkOrLeave().apply(teaOrCoffee));
        return in.nextLine();
    }
}
