package com.company;

import com.company.core.GameApplication;

public class Main {

    public static void main(String[] args) {
        final GameApplication gameApplication = new GameApplication();
        final String name = gameApplication.greeting();
        final String teaOrCoffee = gameApplication.teaOrCoffee(name);
        final String drinkOrLeave = gameApplication.drinkOrLeave(teaOrCoffee);
    }
}
